import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void printStudentLoopAndCheck(List<Student> students) {
        for (Student student : students) {
            if (student.getAge() >= 18 && student.getScore() >= 5) {
                student.print();
            }
        }
    }

    public static void printStudentLoopAndCheckFollowParam(List<Student> students, int age, int score) {
        for (Student student : students) {
            if (student.getAge() >= age && student.getScore() >= score) {
                student.print();
            }
        }
    }

    public static void printStudent(List<Student> students, CheckStudent checkStudent) {
        for (Student student : students) {
            if (checkStudent.test(student))
                student.print();
        }
    }


    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("A", 10, "MALE", 4));
        students.add(new Student("B", 18, "FEMALE", 6));
        students.add(new Student("C", 12, "MALE", 3));
        students.add(new Student("D", 20, "FEMALE", 10));
        students.add(new Student("E", 21, "MALE", 7));
        students.add(new Student("F", 16, "FEMALE", 5));
        printStudentLoopAndCheck(students);
        System.out.println("---------------");
        printStudentLoopAndCheckFollowParam(students, 18, 5);
        System.out.println("---------------");
        // Use local class
        printStudent(students, new CheckStudentImpl());
        System.out.println("---------------");
        // Use anonymous class
        printStudent(students, new CheckStudent() {
            @Override
            public boolean test(Student student) {
                return student.getAge() >= 18 && student.getScore() >= 5;
            }
        });

        System.out.println("---------------");

        // Use lambda expression reduce syntax
        printStudent(students, student -> student.getAge() >= 18 && student.getScore() >= 5);
        System.out.println("---------------");

        // Use lambda expression full syntax
        printStudent(students, student -> {
            boolean result;
            result = student.getAge() >= 18 && student.getScore() >= 5;
            return result;
        });
    }
}
