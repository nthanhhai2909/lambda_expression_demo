public class CheckStudentImpl implements CheckStudent{
    /**
     * Find and print Students has age >= 18 and score >= 5
     * @param student
     */
    @Override
    public boolean test(Student student) {
        return student.getAge() >= 18 && student.getScore() >= 5;
    }
}
