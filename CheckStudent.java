@FunctionalInterface
public interface CheckStudent {
    boolean test(Student student);
}
